let kataNumber = 5;
let kataName = `Kata ${kataNumber}: Choose Your Own Adventure`;
document.title = kataName;
document.getElementById("frontPageH1").innerText = kataName;
// My chosen "adventure": https://edabit.com/challenge/Q3n42rEWanZSTmsJm
// Create a function that takes an array of numbers and return both the minimum and maximum numbers, in that order.

function minAndMaxOfArray(arrayOfNumbers){
    if(!Array.isArray){
        console.error("Not an array");
        return;
    }
    const output = []
    lowestNumber = Math.min(...arrayOfNumbers);
    highestNumber = Math.max(...arrayOfNumbers);
    output.push(lowestNumber);
    output.push(highestNumber);
    return output;
}

const myNumberArray = [1, -3, 55, 20];
console.log(minAndMaxOfArray(myNumberArray));
